import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store


#removing category and deleting th badge

def remove_category(i,category_name):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    updated_product['categories']=[str(x) for x in updated_product['categories']]
    if (clearance_cat_id in updated_product['categories']) or (dow_cat_id in updated_product['categories']):
        updated_product['categories']=[x for x in updated_product['categories'] if str(x) not in [clearance_cat_id,dow_cat_id]]#remove clearance category
        product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
        r = requests.put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
        print 'Product ID/SKU',i['id'],'/',i['sku'],'update status to non-%s %s'%(category_name,str(r.status_code))
    custom_fields = requests.get(base_url+'catalog/products/%s/custom-fields?limit=250'%i['id'],headers=headers)
    custom_fields = json.loads(custom_fields.text)['data']#get all the custome fields
    for field in custom_fields:
        if field['name'].lower() in ['badge','google bidding']:#find badge field
            r_badge = requests.delete(base_url+'catalog/products/%s/custom-fields/%s'%(str(i['id']),str(field['id'])),headers=headers)#delete badge field
            print 'Product ID/SKU',i['id'],'/',i['sku'],'remove custom field %s %s'%(field['name'],str(r_badge.status_code))

start_time=datetime.now()

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash

print '************ Product Clearance Cleaning Report',start_time,'***********'#logging the beginning of the run
print 'Category count request'
r = requests.get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of categories, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(category_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = requests.get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on categories on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

category_data=pd.DataFrame(data=category_data)

clearance_cat_id=str(category_data[category_data['name']=='Clearance']['id'].iloc[0])#get the category ids
dow_cat_id=str(category_data[category_data['name']=='Deal of the Week']['id'].iloc[0])

print 'Product count request',r.status_code#initital request fulfillment report
#marking beginning of the update cycle
r = requests.get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
clearance_added=0
clearance_removed=0
weekly_deals_added=0
weekly_deals_removed=0
largest_discounts_added=0
largest_discounts_removed=0
for page in range(12,page_count+1):
    r = requests.get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)#turn json response into python dictionary
    for product in data['data']:#cycle through products on the page
        remove_category(product,'Any category')
print '******** Finished after',start_time-datetime.now(),'**********\n\n\n'
